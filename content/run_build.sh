#!/usr/bin/env bash

set -e

CONTENT_ROOT="/home/content"

if [ ! $STEAM_ACCOUNT ]
then
    echo "STEAM_ACCOUNT env variable not found."
    exit 1
fi

if [ ! $STEAM_CONFIG_FILE_CONTENT ]
then
    echo "STEAM_CONFIG_FILE_CONTENT env variable not found."
    echo "It should be the content of the config.vdf content encoded in base64."
    echo "It's needed to bypass the Steam's two-factor authentication."
    exit 1
fi

set -x

# Use specific config file to disable 2AF
echo "Copying /root/Steam/config/config.vdf..."
echo "$STEAM_CONFIG_FILE_CONTENT" | base64 -d > "/root/Steam/config/config.vdf"

set +x

# Wrap binary using Steam DRM?
if [ $STEAM_DRM_TOOL ]
then
    if [ ! $BINARY_PATH ]
    then
        echo "BINARY_PATH env variable not found."
        exit 1
    fi

    DRM_FLAGS=$([ $STEAM_DRM_FLAGS ] && echo $STEAM_DRM_FLAGS || echo "0")

    set -x

    ABSOLUTE_BINARY_PATH="${CONTENT_ROOT}/${BINARY_PATH}"

    # Request binary with DRM
    /home/steamcmd.sh \
        +login $STEAM_ACCOUNT \
        +drm_wrap $STEAM_APP_ID "$ABSOLUTE_BINARY_PATH" "$ABSOLUTE_BINARY_PATH" $STEAM_DRM_TOOL $STEAM_DRM_FLAGS \
        +quit
fi

# Upload the build
/home/steamcmd.sh \
    +login $STEAM_ACCOUNT \
    +run_app_build_http ./scripts/app_build.vdf \
    +quit